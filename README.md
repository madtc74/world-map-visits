# World Map Visits

## Name
World Traveler Map

## Description
This is a simple script to show you how to map countries you visited on your journies across the world. 

## Visuals
<https://youtu.be/UDWq5JH2H_Y>

## Installation
You can use this with Jupyter notebooks.

## Authors and acknowledgment
Thanks for the data files and examples to get me started from <https://github.com/python-visualization>
